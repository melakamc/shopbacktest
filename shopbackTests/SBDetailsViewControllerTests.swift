//
//  SBDetailsViewControllerTests.swift
//  shopback
//
//  Created by Melaka Atalugamage on 5/17/17.
//  Copyright © 2017 Melaka Atalugamage. All rights reserved.
//

import XCTest
@testable import ShopBack
@testable import ObjectMapper

class SBDetailsViewControllerTests: XCTestCase {
    
    var detailsViewController:SBDetailsViewController!
    
    override func setUp() {
        super.setUp()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        detailsViewController = storyboard.instantiateViewController(withIdentifier: "MovieDetailsVC") as! SBDetailsViewController
        let _ = detailsViewController.view
        detailsViewController.movie = generateDummyMovieObject()
        detailsViewController.movieDetails = generateDummyMovieDetailsObject()
   
    }

    override func tearDown() {
        detailsViewController = nil
        super.tearDown()
    }

    func testInitialUIDataUpdate() {
        detailsViewController.viewWillAppear(true)
        XCTAssertNotEqual(detailsViewController.lblTitle.text, "loading...")
        XCTAssertNotEqual(detailsViewController.lblPopularity.text, "loading...")
        XCTAssertNotNil(detailsViewController.imgThumbnail.image)
    }
    
    
    func testIsBuyButtonEnable() {
        detailsViewController.viewDidLoad()
        XCTAssertTrue(detailsViewController.btnBuyTickets.isEnabled)
    }
 
    
    func testIsCurtainViewVisible() {
        detailsViewController.viewDidLoad()
        XCTAssertFalse(detailsViewController.curtainView.isHidden)
    }
    
    func testDurationCalculationIsCorrect() {
        let runTime = detailsViewController.movieRunTime(detailsViewController.movieDetails.runtime!)
        XCTAssertEqual(runTime.h, 2)
        XCTAssertEqual(runTime.m, 17)
    }
    
    func testAreOutletsAreProperlyHooked(){
        XCTAssertNotNil(detailsViewController.btnBuyTickets, "Buy ticket button outlet is not hooked up")
        XCTAssertNotNil(detailsViewController.lblTitle, "Title Label outlet is not hooked up")
        XCTAssertNotNil(detailsViewController.lblDuration, "Duration Label outlet is not hooked up")
        XCTAssertNotNil(detailsViewController.lblPopularity, "Popularity Label outlet is not hooked up")
        XCTAssertNotNil(detailsViewController.lblLanguage, "Language Label outlet is not hooked up")
        XCTAssertNotNil(detailsViewController.imgThumbnail, "Thumbnail Image outlet is not hooked up")
        XCTAssertNotNil(detailsViewController.textViewSynopsis, "Synopsis Text view outlet is not hooked up")
    }
    
}

//MARK: Dummy object generation
extension SBDetailsViewControllerTests{
    
    func generateDummyMovieDetailsObject() -> SBMovieDetails {
        let movieDetails = SBMovieDetails(JSON: ["overview":"The Guardians must fight to keep their newfound family together as they unravel the mysteries of Peter Quill's true parentage.",
                                                 "genres":[[
                                                        "id": 35,
                                                        "name": "Comedy"
                                                    ], [
                                                        "id": 28,
                                                        "name": "Action"
                                                    ], [
                                                        "id": 12,
                                                        "name": "Adventure"
                                                    ], [
                                                        "id": 878,
                                                        "name": "Science Fiction"
                                                    ]],
                                                 "runtime":137,
                                                 "spoken_languages":[[
                                                        "iso_639_1": "en",
                                                        "name": "English"
                                                    ],
                                                    [
                                                        "iso_639_1": "fr",
                                                        "name": "French"
                                                    ]]
            ])!
        
        return movieDetails
    }
    
    func generateDummyMovieObject() -> SBMovie {
        let movie = SBMovie(JSON: ["poster_path": "/y4MBh0EjBlMuOzv9axM4qJlmhzz.jpg",
                                   "id":283995,
                                   "original_title":"Guardians of the Galaxy Vol. 2",
                                   "popularity":125.757946])!
        return movie
    }
}
