//
//  SBHomeViewControllerTests.swift
//  shopback
//
//  Created by Melaka Atalugamage on 5/17/17.
//  Copyright © 2017 Melaka Atalugamage. All rights reserved.
//

import XCTest
@testable import ShopBack

class SBHomeViewControllerTests: XCTestCase {
    
    var homeViewController:SBHomeViewController!
    
    override func setUp() {
        super.setUp()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        homeViewController = storyboard.instantiateViewController(withIdentifier: "SBHomeViewController") as! SBHomeViewController
        let _ = homeViewController.view
        homeViewController.movies = generateDummyMovies()
        
    }
    
    override func tearDown() {
        homeViewController = nil
        super.tearDown()
    }
    
    func testTableViewIsVisible() {
        XCTAssertFalse(homeViewController.movieTableView.isHidden)
    }
    
    func testAreOutletsAreProperlyHooked(){
        
        XCTAssertNotNil(homeViewController.movieTableView, "TableView outlet is not hooked up")
    }
    
    func testTableViewDelegateIsSetUp(){
        XCTAssertNotNil(homeViewController.movieTableView.delegate, "TableView delegate is not hooked up")
    }
    
    func testTableViewDatasourceIsSetUp(){
        XCTAssertNotNil(homeViewController.movieTableView.dataSource, "TableView datasource is not hooked up")
    }
    

    func testTableViewHeightForRowAtIndexPath(){
        let expectedHeight:Double = 115.0;
        let actualHeight:Double = Double(homeViewController.movieTableView.rowHeight);
        XCTAssertEqual(expectedHeight, actualHeight, "Cell should have \(expectedHeight) height, but they have \(actualHeight)");
    }
    
}

//MARK: Dummy object generation
extension SBHomeViewControllerTests{
    func generateDummyMovies()->[SBMovie]{
        return [
            SBMovie(JSON: [
                            "poster_path": "/y4MBh0EjBlMuOzv9axM4qJlmhzz.jpg",
                            "id":283995,
                            "original_title":"Guardians of the Galaxy Vol. 2",
                            "popularity":125.757946])!,
            SBMovie(JSON: [
                            "poster_path": "/zpaQwR0YViPd83bx1e559QyZ35i.jpg",
                            "id":425,
                            "original_title":"Ice Age",
                            "popularity":6.668629])!,
            SBMovie(JSON: [
                            "poster_path": "/4YnLxYLHhT4UQ8i9jxAXWy46Xuw.jpg",
                            "id":675,
                            "original_title":"Harry Potter and the Order of the Phoenix",
                            "popularity":6.593133])!,
            SBMovie(JSON: [
                            "poster_path": "/lRjOR4uclMQijUav4OjeZprlehu.jpg",
                            "id":82702,
                            "original_title":"How to Train Your Dragon 2",
                            "popularity":6.492303])!
                ]
    }
}
