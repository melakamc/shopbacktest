//
//  SBRouterTests.swift
//  shopback
//
//  Created by Melaka Atalugamage on 5/17/17.
//  Copyright © 2017 Melaka Atalugamage. All rights reserved.
//

import XCTest
@testable import ShopBack

class SBRouterTests: XCTestCase {
    
    var moviesRouter:SBRouter!
    var detailsRouter:SBRouter!
    
    override func setUp() {
        super.setUp()
        moviesRouter = .movies(page: 1)
        detailsRouter = .details(movieID:283995)
    }
    
    override func tearDown() {
        moviesRouter = nil
        detailsRouter = nil
        super.tearDown()
    }
    
    func testPathReturnsAValue(){
        XCTAssertNotEqual(moviesRouter.path, "")
        XCTAssertNotEqual(detailsRouter.path, "")
    }
    
    func testNoExceptionIsThrown(){
        XCTAssertNoThrow(try moviesRouter.asURLRequest(), "Movies Router URL shouldn't throw an exception")
        XCTAssertNoThrow(try detailsRouter.asURLRequest(), "Details Router URL shouldn't throw an exception")
    }
    
}

