# README #

### How do get set up? ###

* Clone the repo
* Run `pod install` to make sure all pods are properly installed
* Open the `ShopBack.Xcworkspace` and run the project

### Task ###

Test Scenario
 
To help our users discover movies easily,create a movie app with the following screens:
 
1. Home screen with a list of available movies

* Ordered by release date
* Pull to refresh 
* Load when scrolled to bottom 
* Each movie to include: Poster/Backdrop image, Title, Popularity

2. Detail screen 

* Movie details with these additional details: Synopsis, Genres, Language, Duration
* Book the movie(simulate opening of this link in a webview)

It is entirely up to you to design the UI.It is a bonus if you create awesome looking UI, so don’t be too hard on yourself with the design. Simple is good (:

Use the API from TMDb:

* http://api.themoviedb.org/3/discover/movie?api_key=328c283cd27bd1877d9080ccb1604c91&primary_release_date.lte=2016-12-31&sort_by=release_date.desc&page=1
* http://api.themoviedb.org/3/movie/328111?api_key=328c283cd27bd1877d9080ccb1604c91