//
//  SBMovieTableViewCell.swift
//  shopback
//
//  Created by Melaka Atalugamage on 5/13/17.
//  Copyright © 2017 Melaka Atalugamage. All rights reserved.
//

import UIKit

class SBMovieTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPopularity: UILabel!
    @IBOutlet weak var imgThumbnail: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
