//
//  SBRouter.swift
//  shopback
//
//  Created by Melaka Atalugamage on 5/13/17.
//  Copyright © 2017 Melaka Atalugamage. All rights reserved.
//

import Foundation
import Alamofire

public  enum SBRouter: URLRequestConvertible {

 
    case movies(page:Int)
    case details(movieID:Int)
    
    var method: HTTPMethod {
        switch self {
        case .movies, .details:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .movies:
            return "discover/movie"

        case .details(let movieID):
            return "movie/\(movieID)"
        }
    
    }
    
    public func asURLRequest() throws -> URLRequest {
        let parameters: [String: Any] = {
            switch self {
            case .movies(let page):
                return ["api_key":SBGlobalConstants.API_KEY, "page":page]
            case .details( _):
                return ["api_key":SBGlobalConstants.API_KEY]
            }
        }()
        
        let url = try SBGlobalConstants.API_BASE_URL.asURL()
        
        var request = URLRequest(url: url.appendingPathComponent(path))
        request.httpMethod = method.rawValue
        request.timeoutInterval = TimeInterval(10 * 1000)
 
        return try URLEncoding.default.encode(request, with: parameters)
    }
}
