//
//  SBMovieDetails.swift
//  shopback
//
//  Copyright © 2017 Melaka Atalugamage. All rights reserved.
//

import UIKit

import ObjectMapper



class SBMovieDetails: Mappable {
    
    var overview: String?
    var genres: [SBMovieGenre]?
    var languages: [SBMovieLanguage]?
    var runtime: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        overview    <- map["overview"]
        genres      <- map["genres"]
        languages   <- map["spoken_languages"]
        runtime     <- map["runtime"]
    }
    
    
    
}

class SBMovieGenre: Mappable{
    var genreID:NSNumber?
    var name:String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        genreID     <- map["id"]
        name        <- map["name"]
    }
    
}

class SBMovieLanguage: Mappable{
    var isoID:String?
    var name:String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        isoID     <- map["iso_639_1"]
        name        <- map["name"]
    }
    
}



