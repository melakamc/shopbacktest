//
//  DataFetcher.swift
//  shopback
//
//  Created by Melaka Atalugamage on 5/12/17.
//  Copyright © 2017 Melaka Atalugamage. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

public class SBDataFetcher: NSObject {
    
     static func fetchMoviesIn(page:Int, withCallback callback:@escaping (_ movies:[SBMovie]?,_ error:Error?)-> ())->Void{

        Alamofire.request(SBRouter.movies(page: page)).responseObject { (response: DataResponse<SBMovieResponse>) in
            if let error = response.error{
                callback(nil,error)
                return;
            }
            let movieResponse = response.result.value
            if let movieArray = movieResponse?.movies {
                 callback(movieArray, nil)
            }
        }
    }
    
    static func fetchMovieDetails(movieID:Int, withCallback callback:@escaping (_ movieDetails:SBMovieDetails?,_ error:Error?)-> ())->Void{
        Alamofire.request(SBRouter.details(movieID: movieID)).responseObject { (response: DataResponse<SBMovieDetails>) in
            if let error = response.error{
                callback(nil,error)
                return;
            }
            
            let movieDetailsResponse = response.result.value
            callback(movieDetailsResponse,nil)
            
        }
      
        
    }

}
