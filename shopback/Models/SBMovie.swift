//
//  SBMovie.swift
//  shopback
//
//  Created by Melaka Atalugamage on 5/13/17.
//  Copyright © 2017 Melaka Atalugamage. All rights reserved.
//

import UIKit
import ObjectMapper

class SBMovieResponse: Mappable{
    var page: Int?
    var movies: [SBMovie]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        page <- map["location"]
        movies <- map["results"]
    }
}

class SBMovie: Mappable {
    
    var posterPath: String?
    var movieID: Int?
    var originalTitle: String?
    var popularity: Double?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        posterPath      <- map["poster_path"]
        movieID         <- map["id"]
        originalTitle   <- map["original_title"]
        popularity      <- map["popularity"]
    }
    
    func getPosterImageURL() -> URL?{
        if let path = self.posterPath {
            let url = URL(string:(SBGlobalConstants.API_IMAGE_BASE_URL + path))
            return url
        }else{
            return nil
        }
    }
    

}


