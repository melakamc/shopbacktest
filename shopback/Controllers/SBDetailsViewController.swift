//
//  SBDetailsViewController.swift
//  shopback
//
//  Created by Melaka Atalugamage on 5/16/17.
//  Copyright © 2017 Melaka Atalugamage. All rights reserved.
//

import UIKit

class SBDetailsViewController: SBBaseViewController {
    var movie:SBMovie!
    var movieDetails:SBMovieDetails!
    
    @IBOutlet weak var imgThumbnail: UIImageView!
    @IBOutlet weak var lblPopularity: UILabel!
    @IBOutlet weak var lblLanguage: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblGenres: UILabel!
    @IBOutlet weak var textViewSynopsis: UITextView!
    @IBOutlet weak var btnBuyTickets: UIButton!
    @IBOutlet weak var curtainView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateUIWithLocalData()
        if let movie = self.movie {
            fetchDetailsOf(movieID: movie.movieID!)
        }
        self.title = movie.originalTitle
    }
    

    @IBAction func buyButtonTapped(_ sender: UIButton) {
        let webURL = URL(string:SBGlobalConstants.BUY_BUTTON_WEB_URL)!
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc:SBPurchaseTicketsViewController = storyboard.instantiateViewController(withIdentifier: "PurchaseTicketsVC") as! SBPurchaseTicketsViewController
        vc.webURL = webURL
        self.navigationController?.pushViewController(vc, animated: true)
    }
  


}

//MARK: Data fetching and displaying logic
extension SBDetailsViewController{
    
    func fetchDetailsOf(movieID:Int) -> Void {
        self.showActivityIndicator(inView: self.view)
        SBDataFetcher.fetchMovieDetails(movieID: movieID) { [weak self](_movieDetails: SBMovieDetails?, _error:Error?) in
            self?.hideActivityIndicator()
            
            if let error = _error{
                self?.showMessage(error.localizedDescription, type: .error)
                return
            }
            
            self?.curtainView.isHidden = true
            self?.movieDetails = _movieDetails
            self?.updateUIWithFetchedData()
        }
    }
    
    func updateUIWithLocalData() -> Void {
        self.lblTitle.text = self.movie.originalTitle
        if let popularity = movie?.popularity {
            self.lblPopularity.text = String(format:"%.1f",popularity)
        }
        if let imageURL = self.movie.getPosterImageURL() {
            self.imgThumbnail.sd_setImage(with: imageURL , placeholderImage: nil)
        }
    }
    
    func updateUIWithFetchedData() -> Void {
        
        let runtime = movieRunTime((self.movieDetails.runtime)!)
        self.lblDuration.text = "\(runtime.h)hours \(runtime.m)mins"
        self.textViewSynopsis.text = self.movieDetails.overview
        let genresString = (movieDetails.genres)!.map({$0.name!}).joined(separator:", ")
        self.lblGenres.text = genresString
        let languageString = (movieDetails.languages)!.map({$0.name!}).joined(separator:", ")
        self.lblLanguage.text = languageString
        
    }
    
    func movieRunTime(_ duration:Int) -> (h:Int,m:Int) {
        
        return (duration / 60, (duration % 60) )
    }
}
