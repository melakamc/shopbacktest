 //
//  SBHomeViewController.swift
//  shopback
//
//  Copyright © 2017 Melaka Atalugamage. All rights reserved.
//

import UIKit
import SDWebImage
import DGElasticPullToRefresh

 

class SBHomeViewController: SBBaseViewController {
 
    @IBOutlet weak var movieTableView: UITableView!
    var movies:[SBMovie] = Array()
    let cellReuseIdentifier = "SBMovieTableViewCell"
    var pageCount = 1
    var isPageEnd = false
    let loadingView = DGElasticPullToRefreshLoadingViewCircle()

    override func viewDidLoad() {
        super.viewDidLoad()
        initPullDownToRefresh()
        fetchMovies(clearingCache: false)
        
       

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    deinit {
        movieTableView.dg_removePullToRefresh()
    }


}
 
// MARK: TableView Datasource & Delegate
 extension SBHomeViewController:UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! SBMovieTableViewCell
        
        
        let movie = movies[indexPath.row]
        
        cell.lblName.text = movie.originalTitle
        if let popularity = movie.popularity {
            cell.lblPopularity.text = String(format:"Popularity %.1f",popularity)
        }
        if let imageURL = movie.getPosterImageURL() {
            cell.imgThumbnail.sd_setImage(with: imageURL , placeholderImage: nil)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 115
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = movies.count - 5
        if indexPath.row == lastElement && !self.isPageEnd{
            fetchMovies(clearingCache: false)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let movie = movies[indexPath.row]
        presentDetailsVC(movie)
    }
    
    func presentDetailsVC(_ movie:SBMovie) -> Void {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc:SBDetailsViewController = storyboard.instantiateViewController(withIdentifier: "MovieDetailsVC") as! SBDetailsViewController
        vc.movie = movie
        self.navigationController?.pushViewController(vc, animated: true)
    }
 }
 
 // MARK: Pull down to refresh
 extension SBHomeViewController{

    func initPullDownToRefresh() -> Void {
         self.movieTableView.dg_addPullToRefreshWithActionHandler({ [unowned self] () -> Void in
           
            self.pageCount = 1
            self.fetchMovies(clearingCache: true)
         }, loadingView: loadingView)
        
        self.movieTableView.dg_setPullToRefreshFillColor(UIColor(red: 57/255.0, green: 67/255.0, blue: 89/255.0, alpha: 1.0))
        self.movieTableView.dg_setPullToRefreshBackgroundColor(self.movieTableView.backgroundColor!)
    }
 
 }
 
 // MARK: Data fetching logic
 extension SBHomeViewController{
    
    
    func fetchMovies(clearingCache:Bool)->Void{
        if clearingCache {
            URLCache.shared.removeAllCachedResponses()
        }
        self.showActivityIndicator(inView: movieTableView)
        SBDataFetcher.fetchMoviesIn(page:self.pageCount){ [weak self]  (_movies:[SBMovie]?, _error:Error?) in
            self?.hideActivityIndicator()
            self?.movieTableView.dg_stopLoading()
            
            if let error = _error{
                self?.showMessage(error.localizedDescription, type: .error)
                return
            }
            
            if let movies = _movies{
                
                if movies.count == 0{
                    self?.isPageEnd = true
                }
                
                if(self?.pageCount == 1 ){
                    self?.movies = movies
                }else{
                    self?.movies += movies
                }
            }
        
            
           
            
            self?.pageCount += 1
            self?.movieTableView.reloadData()
            
        }
    }
    
 }

