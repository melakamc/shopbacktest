//
//  SBPurchaseTicketsViewController.swift
//  shopback
//
//  Created by Melaka Atalugamage on 5/16/17.
//  Copyright © 2017 Melaka Atalugamage. All rights reserved.
//

import UIKit

class SBPurchaseTicketsViewController: SBBaseViewController {
    @IBOutlet weak var webView: UIWebView!
    var webURL:URL? = nil

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let urlRequest:URLRequest = URLRequest(url: webURL!)
        webView.loadRequest(urlRequest)
        self.showActivityIndicator(inView: webView)
    }

}

extension SBPurchaseTicketsViewController:UIWebViewDelegate{
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.hideActivityIndicator()
    }
}
