//
//  SBBaseViewController.swift
//  shopback
//
//  Copyright © 2017 Melaka Atalugamage. All rights reserved.
//

import UIKit
import SwiftMessages

enum SBMessageType{
    case error
    case success
    case info
}

class SBBaseViewController: UIViewController {
    var activityIndicator:UIActivityIndicatorView? = nil
    
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.tintColor = UIColor(red:0.50, green:0.54, blue:0.66, alpha:1.00)
    }

  
    func showActivityIndicator(inView view:UIView) {
        if self.activityIndicator == nil {
            initActivityIndicator()
        }
        self.activityIndicator?.center = view.center
        view.addSubview(self.activityIndicator!)
        self.activityIndicator?.startAnimating()
    }
    
    func hideActivityIndicator() {
        self.activityIndicator?.removeFromSuperview()
        self.activityIndicator?.stopAnimating()
    }
    
    func initActivityIndicator() {
        self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        self.activityIndicator?.color = UIColor(red:0.50, green:0.54, blue:0.66, alpha:1.00)
    }
    
    func showMessage(_ message:String, type:SBMessageType){
        
        let view = MessageView.viewFromNib(layout: .CardView)
        
        switch type {
        case .error:
            view.configureTheme(.error)
        case .success:
            view.configureTheme(.success)
        case .info:
            view.configureTheme(.info)
        }
        
        view.button?.isHidden = true
        view.configureDropShadow()
        view.configureContent(title: "ShopBack", body: message)
        SwiftMessages.show(view: view)
    }
    
    func hideMessages() {
        SwiftMessages.hideAll()
    }

}
