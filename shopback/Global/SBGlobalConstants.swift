//
//  SBGlobalConstants.swift
//  shopback
//
//  Created by Melaka Atalugamage on 5/13/17.
//  Copyright © 2017 Melaka Atalugamage. All rights reserved.
//

import UIKit

public struct SBGlobalConstants {
    static let API_BASE_URL = "http://api.themoviedb.org/3/"
    static let API_KEY = "328c283cd27bd1877d9080ccb1604c91"
    static let API_IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w500/"
    static let BUY_BUTTON_WEB_URL = "http://www.cathaycineplexes.com.sg/"
}
